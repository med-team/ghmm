## GHMM test in python

Test in python can be referenced [here](https://ghmm.sourceforge.net/documentation.html)

## GHMM test in C

The following tests were skipped due to segfault:
-   `test_mcmc`
-   `test_chmm`