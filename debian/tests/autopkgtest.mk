CC=gcc
CFLAGS=-I/usr/include/ghmm
LDFLAGS=-lghmm -llapack


all: test_label_higher_order test_two_states_three_symbols test_libxml test_read_fa test_sequences test_chmm test_chmm_test test_coin_toss test_mcmc test_randvar

test_label_higher_order: label_higher_order_test.o
	$(CC) $^ -o $@ $(LDFLAGS) -lm

test_two_states_three_symbols: two_states_three_symbols.o
	$(CC) $^ -o $@ $(LDFLAGS)

test_libxml: libxml-test.o
	$(CC) $^ -o $@ $(LDFLAGS)

test_read_fa: read_fa.o
	$(CC) $^ -o $@ $(LDFLAGS)

test_sequences: sequences_test.o
	$(CC) $^ -o $@ $(LDFLAGS)

test_chmm: chmm.o
	$(CC) $^ -o $@ $(LDFLAGS)

test_chmm_test: chmm_test.o
	$(CC) $^ -o $@ $(LDFLAGS)

test_coin_toss: coin_toss_test.o
	$(CC) $^ -o $@ $(LDFLAGS)

test_mcmc: mcmc.o
	$(CC) $^ -o $@ $(LDFLAGS)

test_randvar: randvar_test.o
	$(CC) $^ -o $@ $(LDFLAGS)

%.o: %.c
	$(CC) -c $< -o $@ $(CFLAGS)

clean:
	rm -f *.o test_* words.txt

run: all
	./test_label_higher_order
	./test_two_states_three_symbols
	./test_libxml
	./test_sequences
	./test_chmm_test
	./test_coin_toss
	./test_randvar
	# read_fa needs seqence and model to be passed as arguments
	# ./test_read_fa 
	# ./test_chmm and ./test_mcmc returns SEGFAULT