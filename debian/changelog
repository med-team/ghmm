ghmm (0.9~rc3-11) unstable; urgency=medium

  * Team Upload.
  * Fixup build with python 3.13 (Closes: #1092058)

 -- Nilesh Patra <nilesh@debian.org>  Tue, 14 Jan 2025 18:47:58 +0530

ghmm (0.9~rc3-10) unstable; urgency=medium

  * Team upload.
  * Fix Python3.12 string syntax
    Closes: #1085608

 -- Andreas Tille <tille@debian.org>  Sat, 07 Dec 2024 12:01:01 +0100

ghmm (0.9~rc3-9) unstable; urgency=medium

  * d/control: setup.pys depend on python3-setuptools. (Closes: #1080603)
  * d/*lintian-overrides: freshen time64_t overrides.

 -- Étienne Mollier <emollier@debian.org>  Thu, 05 Sep 2024 21:54:00 +0200

ghmm (0.9~rc3-8) unstable; urgency=medium

  * buildError.patch: unfuzz.
  * d/control: add myself to uploaders.
  * d/control: declare compliance to standards version 4.7.0.
  * gcc-14.patch: new: fix a pointer declaration in tests/mcmc.c.
    (Closes: #1074994)
  * d/s/lintian-overrides: fix the t64 migration override.
  * d/rules: convert an ISO source file to UTF-8.
  * d/rules: disallow execution of an example source file.

 -- Étienne Mollier <emollier@debian.org>  Tue, 16 Jul 2024 22:25:11 +0200

ghmm (0.9~rc3-7) unstable; urgency=medium

  * Team upload.
  * Add fix_imp_func_issue.patch to fix ftbfs issue.(Closes: #1073355)
  * Modify override_dh_install to match more python version in mv cmd

 -- Bo YU <tsu.yubo@gmail.com>  Thu, 27 Jun 2024 10:56:57 +0800

ghmm (0.9~rc3-6) unstable; urgency=medium

  * Team upload.
  * fix-test-mcmc.patch: new: fix missing imports. (Closes: #1065974)
  * python3.12.patch: new: fix remaining failures.
    There were a couple of occurrences of calls to obsolet APIs causing
    build failures.
  * d/control: move to pkgconf.
  * d/s/lintian-overrides: new: hide new error.
    The lintian error looks to be a side effect of necessary changes for
    the 64-bit time_t transition.

 -- Étienne Mollier <emollier@debian.org>  Tue, 12 Mar 2024 23:02:56 +0100

ghmm (0.9~rc3-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062427

 -- Steve Langasek <vorlon@debian.org>  Wed, 28 Feb 2024 17:43:09 +0000

ghmm (0.9~rc3-5.1~exp) experimental; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.
  * Bump build-dependency on d-shlibs for --t64 support.

 -- Lukas Märdian <slyon@debian.org>  Wed, 07 Feb 2024 18:14:34 +0100

ghmm (0.9~rc3-5) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Standards-Version: 4.6.2 (routine-update)
  * Avoid explicitly specifying -Wl,--as-needed linker flag.
  * Fix: SyntaxWarning: "is" with a literal. Did you mean "=="?
  * Fix clean target
    Closes: #1044649
  * Install additional header files needed for autopkgtest
  * Explain in README.Debian that Python3 interface is broken

  [ Komolehin Israel Timilehin ]
  * Add autopkgtest

  [ Sébastien Villemot ]
  * Remove build-dependency on libatlas-base-dev
    Closes: #1056673

 -- Andreas Tille <tille@debian.org>  Wed, 07 Feb 2024 15:25:59 +0100

ghmm (0.9~rc3-4) unstable; urgency=medium

  * Team upload.
  * Fix homepage
    Closes: #980934

 -- Andreas Tille <tille@debian.org>  Sun, 24 Jan 2021 20:27:28 +0100

ghmm (0.9~rc3-3) unstable; urgency=medium

  * Team upload.
  * Use 2to3 to port to Python3
    Closes: #936609
  * Use secure URI in debian/watch.
  * Set upstream metadata fields: Archive.
  * Add files missing in installation
  * Fix configure options
  * Add missing Build-Depends
  * Add Upstream-Contact
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * No tab in license text (routine-update)
  * Set upstream metadata fields: Repository.
  * Drop `--enable-gsl` from d/rules (thanks for the hint to Frederic-Emmanuel
    PICCA)

 -- Andreas Tille <tille@debian.org>  Thu, 17 Dec 2020 11:29:27 +0100

ghmm (0.9~rc3-2) unstable; urgency=medium

  [ Chris Lamb ]
  * Enable reproducible build by removing absolute build path from ghmm-config
    Closes: #929791

  [ Andreas Tille ]
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Sun, 02 Jun 2019 15:13:30 +0200

ghmm (0.9~rc3-1) unstable; urgency=medium

  * Team upload
  * Initial release (Closes: #899396)

 -- Andreas Tille <tille@debian.org>  Thu, 06 Dec 2018 11:57:15 +0100
