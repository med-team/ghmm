#!/usr/bin/make -f

export DH_VERBOSE = 1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

export PYBUILD_NAME=HMMEd

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure --sourcedirectory=$(CURDIR) -- --enable-lapacke
	dh_auto_configure --buildsystem=pybuild --sourcedirectory=HMMEd

override_dh_auto_build:
	dh_auto_build
	dh_auto_build --buildsystem=pybuild --sourcedirectory=HMMEd

override_dh_auto_install:
	dh_auto_install
	dh_auto_install --buildsystem=pybuild --sourcedirectory=HMMEd

override_dh_install:
	d-shlibmove --commit \
		    --t64 \
		    --multiarch \
		    --devunversioned \
		    --exclude-la \
		    --override s/libgsl23-dev/libgsl-dev/ \
		    --override s/liblapacke3-dev/liblapacke-dev/ \
		    --movedev debian/tmp/usr/include usr \
		    debian/tmp/usr/lib/*/*.so
	dh_install
	cd $(CURDIR)/debian/ghmm/usr/bin/ && mv cluster ghmm-cluster
	sed -i -e 's@ [^ ]*-f\(file\|debug\)-prefix-map=[^ ]*@@g' \
		$(CURDIR)/debian/ghmm/usr/bin/ghmm-config
	find debian -name libghmm.la -delete
	# temporarily copy over ghmmwrapper to the pybuild created dir
	mkdir -p debian/python3-HMMEd/usr/lib/python3/dist-packages
	mv debian/tmp/usr/local/lib/python3.*/dist-packages/ghmmwrapper-0.8-py3*.egg debian/python3-HMMEd/usr/lib/python3/dist-packages/ghmm
	rm -rf debian/tmp/usr/local
	find debian -type d -name __pycache__ | xargs rm -rf

override_dh_python3:
	# We do not provide a package python3-HMMEd thus copy the pybuild installed files over to ghmm
	mv debian/python3-HMMEd/usr/bin/* debian/ghmm/usr/bin
	mv debian/python3-HMMEd/usr/lib debian/ghmm/usr
	dh_python3 -p ghmm

override_dh_auto_clean:
	if [ -r Makefile ]; then $(MAKE) clean; fi
	find . -name Makefile -delete
	find . \( -name "*.o" -o -name "*.lo" \) -delete
	rm -f ghmmwrapper/ghmmwrapper.py ghmmwrapper/ghmmwrapper_wrap.c
	rm -rf HMMEd/build/ ghmmwrapper/build/ HMMEd/HMMEd.egg-info HMMEd/dist
	rm -rf ghmmwrapper/ghmmwrapper.egg-info ghmmwrapper/dist
	rm -f config.h config.log config.status
	rm -f libtool stamp-h1 tests/chmm_test.2JNcXz
	rm -f tools/ghmm-config
	rm -rf ghmm/.deps/ tests/.deps/ tools/.deps/
	rm -rf ghmm/ghmmconfig.h

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	$(MAKE) --directory=tests
endif

execute_after_dh_installexamples:
	# convert sequences_test.c from its national encoding
	iconv -f ISO-8859-1 -t UTF-8 tests/sequences_test.c \
		-o debian/ghmm/usr/share/doc/ghmm/examples/sequences_test.c

execute_before_dh_fixperms:
	chmod -v -x debian/ghmm/usr/share/doc/ghmm/examples/*.c
